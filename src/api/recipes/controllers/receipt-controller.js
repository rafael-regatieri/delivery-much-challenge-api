'use strict';

import _ from 'lodash';
import HTTPStatus from 'http-status';
import Services from '../services/receipt-service.js';

class RecipesController {
  /**
   * Method responsible for fetching receipts
   * @param {Object} request - Object with request data
   * @returns {Object} reply return payload object
   */
  async get (request, reply) {
    const service = new Services();
    let response = reply.response({});
    response.type('application/json');
    const resultReceipts = [];
    const resultIngredients = [];
    const options = copyRequest(request);
    const queryParams = options.query.i;
    const keys = queryParams.split(',');

    if (keys.length > 3) {
      response = reply.response({ error: 'You must submit a maximum of three ingredients' });
      response.code(HTTPStatus.BAD_REQUEST);
      return response;
    }

    const receipts = await service.getRecipepuppy(options.query.i);

    if (!_.has(receipts.data, 'results') || receipts.data.results.length === 0) {
      response = reply.response({ error: 'receipts not found' });
      response.code(HTTPStatus.NOT_FOUND);
      return response;
    }

    for (const ing of receipts.data.results) {
      const gifs = await service.getGiphyGif(ing.title.replace('&#233;', 'é'));
      ing.gif = _.has(gifs, 'data.data[0].url') ? gifs.data.data[0].url : 'not found';
      resultReceipts.push(ing);
    }

    _.forEach(resultReceipts, function (value) {
      resultIngredients.push(buildResponse(value));
    });

    const result = {
      keywords: keys,
      recipes: resultIngredients
    };

    response = reply.response(result);
    response.code(HTTPStatus.OK);
    return response;
  }
}

/**
   * Method responsible for copy data request
   * @param {Object} request - Object with request data
   * @returns {Object} reply return payload object
*/
function copyRequest (request) {
  return {
    headers: _.cloneDeep(request.headers),
    query: _.cloneDeep(request.query),
    params: _.cloneDeep(request.params),
    payload: _.cloneDeep(request.payload)
  };
}

/**
   * Method responsible create a build request
   * @param {Object} request - Object with request data
   * @returns {Object} reply return payload object
*/
function buildResponse (receipts) {
  return {
    title: receipts.title,
    ingredients: receipts.ingredients.split(','),
    link: receipts.href,
    gif: receipts.gif
  };
}

export default RecipesController;
