'use strict';

import Joi from 'joi';
import Controller from '../controllers/receipt-controller.js';
const controller = new Controller();

const routes = [
  {
    method: 'GET',
    path: '/recipes/',
    options: {
      handler: controller.get,
      description: 'Route responsible for returning recipes and ingredients',
      notes: 'Returns a recipe',
      tags: ['api'],
      validate: {
        query: Joi.object({
          i: Joi.string().description('Recipe ingredients').example('onion,tomato').required()
        })
      }
    }
  }
];

export default routes;
